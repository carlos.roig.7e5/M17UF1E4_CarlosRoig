using UnityEngine;


namespace Scripts
{
    public enum Movement
    {
        MoveLeft,
        MoveRight,
        Stop
    }

    public enum Stat
    {
        Normal,
        Giant
    }
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField]
        private int _frameIndex;
        private SpriteRenderer _sr;
        [SerializeField]
        private float _counter;
        [SerializeField]
        private float _countdown;
        private float _habTimeLimit;
        public float HabTimeLimit
        {
            get => _habTimeLimit;
            set => _habTimeLimit = value;
        }
        [SerializeField]
        private float _frameRate;
        private bool _stopAnimation;
        public float FrameRate
        {
            get => _frameRate;
            set => _frameRate = value;
        }
        [SerializeField]
        private float _waitTime;
        [SerializeField]
        private float _countWaitTime;
        private PlayerData _playerData;
        [SerializeField]
        private float _frame;
        private Movement _movement;
        private Stat _stat;
        // Start is called before the first frame update
        void Start()
        {
            _sr = gameObject.GetComponent<SpriteRenderer>();
            _counter = 0;
            _frameIndex = 0;
            _movement = Movement.MoveLeft;
            _stat = Stat.Normal;
            _countdown = 0;
            _habTimeLimit = 10;
            _frame = 0;
            _countWaitTime = 0;
            _playerData = gameObject.GetComponent<PlayerData>();
            _playerData.InitHeight = _playerData.Height;
            _playerData.GiantHeight = _playerData.InitHeight * _playerData.AugHeight;
            transform.position = new Vector3(_playerData.InitPos, transform.position.y, transform.position.z);
        }

        // Update is called once per frame
        void Update()
        {
            if (_frameRate != 0)
            {
                _stopAnimation = false;
                PlayerActiveGiant(ref _stat);
                if (_counter >= 1 / _frameRate)
                {
                    FlipChar(transform.position.x);
                    switch (_movement)
                    {
                        case Movement.MoveLeft:
                            ChangePositionBySpeed(_playerData.Speed);
                            break;
                        case Movement.MoveRight:
                            ChangePositionBySpeed(_playerData.Speed * -1);
                            break;
                        case Movement.Stop:
                            _stopAnimation = true;
                            break;
                    }
                    _frame++;
                    if (_frame % _playerData.Sprites.Length == 0 && !_stopAnimation)
                    {
                        _sr.sprite = _playerData.Sprites[_frameIndex];
                        _frameIndex = _frameIndex >= _playerData.Sprites.Length - 1 ? 0 : _frameIndex + 1;
                    }
                    GiantHability();
                }
                if (_counter >= 1 / _frameRate)
                    _counter = 0;
                if (_frame >= _frameRate)
                    _frame = 0;
                _counter += Time.deltaTime;
            }


        }

        void GiantHability()
        {
            if (_stat == Stat.Giant)
            {
                Tranform(0.2f, 1 / FrameRate, _playerData.Height, _playerData.GiantHeight);
                if (_countdown >= _habTimeLimit)
                    _stat = Stat.Normal;
            }
            if (_stat == Stat.Normal)
                Tranform(-0.2f, -1 / FrameRate, _playerData.InitHeight, _playerData.Height);
        }
        void Tranform(float mod, float time, float minorValue, float maxValue)
        {
            if (minorValue < maxValue)
            {
                ChangeVariable(mod, ref _playerData.Height);
                ChangeVariable(mod, ref _playerData.Weight);
            }
            else
            {
                _countdown += time;
            }
        }

            void ChangePositionBySpeed(float speed)
            {
                transform.position = new Vector3(transform.position.x + speed / _playerData.Weight * _counter, transform.position.y, transform.position.z);
            }

            void PlayerActiveGiant(ref Stat stat)
            {
                if (Input.GetKey(KeyCode.G))
                {
                    if (_countdown <= 0)
                        stat = Stat.Giant;
                }
            }

            void ChangeVariable(float mod, ref float variable)
            {
                variable += mod;
            }

            void FlipChar(float positon)
            {
                //Turn to left
                if (positon <= _playerData.InitPos)
                    StartPosition(Movement.MoveLeft, true);

                //Turn to right
                if (positon >= _playerData.FinalPos)
                    StartPosition(Movement.MoveRight, false);
            }

            void StartPosition(Movement axis, bool flipDirection)
            {
                _sr.flipX = flipDirection;
                _movement = Movement.Stop;
                if (WaitTime())
                    _movement = axis;
            }
            bool WaitTime()
            {
                if (_countWaitTime < _waitTime)
                {
                    _countWaitTime += 1 / FrameRate;
                    return false;
                }
                _countWaitTime = 0;
                return true;
            }
        }
    }

