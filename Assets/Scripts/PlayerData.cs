using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class PlayerData : MonoBehaviour
    {
        public string Name;
        public float Speed;
        public float Weight;
        public float Height;
        public float InitHeight;
        public float GiantHeight;
        public float AugHeight;
        public float FinalPos;
        public float InitPos;
        public Sprite[] Sprites;
        // Start is called before the first frame update
        void Start()
        {
            transform.localScale = new Vector3(Height, Height, transform.localScale.z);
            Name = GameObject.Find("PlayerName").GetComponent<Text>().text;
        }

        // Update is called once per frame
        void Update()
        {
            GiantHeight = InitHeight * AugHeight;
            transform.localScale = new Vector3(Height, Height, transform.localScale.z);
        }
    }
}
