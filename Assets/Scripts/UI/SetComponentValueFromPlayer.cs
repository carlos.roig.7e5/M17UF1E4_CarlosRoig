using UnityEngine;
using TMPro;
using Scripts;
using System;
public class SetComponentValueFromPlayer : MonoBehaviour
{
    private PlayerMovement _playerMovement;
    private PlayerData _playerData;
    public Component component;
    // Start is called before the first frame update
    void Start()
    {
        _playerMovement = GameObject.Find("Player").GetComponent<PlayerMovement>();
        _playerData = GameObject.Find("Player").GetComponent<PlayerData>();
    }
    float SetScriptValue()
    {
        return Convert.ToInt32(gameObject.GetComponent<TMP_Text>().text);
    }
    void Update()
    {
       
        switch (component)
        {
            case Component.GiantTime:
                _playerMovement.HabTimeLimit = SetScriptValue();
                break;
            case Component.Speed:
                _playerData.Speed = SetScriptValue();
                break;
            case Component.FinalPos:
                _playerData.FinalPos = SetScriptValue();
                break;
            case Component.InitPos:
                _playerData.InitPos = SetScriptValue();
                break;
        }
    }
}
