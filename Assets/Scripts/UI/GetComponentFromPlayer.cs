using UnityEngine;
using TMPro;
using Scripts;

public enum Component
{
    Frames,
    FinalPos,
    InitPos,
    GiantTime,
    Speed, 
    Name
}
public class GetComponentFromPlayer : MonoBehaviour
{
    private PlayerMovement _playerMovement;
    private PlayerData _playerData;
    public Component component;
    // Start is called before the first frame update
    void Start()
    {
        _playerMovement = GameObject.Find("Player").GetComponent<PlayerMovement>();
        _playerData = GameObject.Find("Player").GetComponent<PlayerData>();
    }

    void GetComponentValue( string value)
    {
        gameObject.GetComponent<TMP_Text>().text = value;
    }

    // Update is called once per frame
    void Update()
    {        

        switch (component)
        {
            case Component.Frames:
                GetComponentValue("Frames : " + _playerMovement.FrameRate);
                break;
            case Component.GiantTime:
                GetComponentValue("" + _playerMovement.HabTimeLimit);
                break;
            case Component.Speed:
                GetComponentValue("" + _playerData.Speed);
                break;
            case Component.FinalPos:
                GetComponentValue( "" + _playerData.FinalPos);
                break;
            case Component.InitPos:
                GetComponentValue("" + _playerData.InitPos);
                break;
            case Component.Name:
                GetComponentValue("Name : " + _playerData.Name);
                break;
        }
    }
}
