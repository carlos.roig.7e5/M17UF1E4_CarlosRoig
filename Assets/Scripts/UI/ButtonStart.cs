using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
public class ButtonStart : MonoBehaviour
{
    [SerializeField]
    private string _scene;
    private GameObject _playerName;
    void Start()
    {
        _playerName = GameObject.Find("PlayerName");
    }
    public void TaskOnClick()
    {
        if (_playerName.GetComponent<Text>().text.Length > 2)
            SceneManager.LoadScene(_scene, LoadSceneMode.Single);
        else
            gameObject.GetComponent<Pop_Up>().StartPop_Up();
    }
   

}
